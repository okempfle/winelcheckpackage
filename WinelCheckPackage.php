<?php

namespace WinelCheckPackage;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class WinelCheckPackage extends Plugin
{

    public function build(ContainerBuilder $container)
    {
        $container->setParameter('winel_check_package.plugin_dir', $this->getPath());
        $container->setParameter('winel_check_package.plugin_name', $this->getName());
        parent::build($container);
    }

    public function install(InstallContext $context)
    {

        parent::install($context);
    }

    public function update(UpdateContext $context)
    {

        parent::update($context);
    }

    public function uninstall(UninstallContext $context)
    {

        parent::uninstall($context);
    }

}