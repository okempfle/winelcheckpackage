<?php

namespace WinelCheckPackage\Subscriber;

use Enlight\Event\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CheckoutSubscriber implements SubscriberInterface
{

    private $container;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

    }

    private function getName()
    {
        return $this->container->getParameter('winel_check_package.plugin_name');
    }
    
      private function getPluginPath()
    {
        return $this->container->getParameter('winel_check_package.plugin_dir');
    }
    
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_Checkout' => 'onPostDispatch',
            'Shopware_Controllers_Frontend_Checkout::finishAction::after' => 'onSaveOrder'
        ];
    }

     
    public function onSaveOrder(\Enlight_Hook_HookArgs $args)
    {
        $shop = $this->container->get('shop');
        $config = $this->container->get('shopware.plugin.config_reader')->getByPluginName($this->getName(), $shop);
        if (!$config['active']) {
            return;
        }
         $controller = $args->getSubject();
        $view = $controller->View();

        $post = $controller->Request()->getParams();

     }
     



    public function onPostDispatch(\Enlight_Event_EventArgs $args)
    {
     $shop = $this->container->get('shop');
        $config = $this->container->get('shopware.plugin.config_reader')->getByPluginName($this->getName(), $shop);
        if (!$config['active']) {
            return;
        }
        $controller = $args->getSubject();
        $view = $controller->View();
        $basket = $view->getAssign('sBasket');
        $userId = $controller->get('session')->offsetGet('sUserId');

        $this->checkPackage($basket);
    }



    public function checkPackage($basket){
      
        foreach ($basket['content'] as $id => $artikel){

        }
        return $basket;
    }




}